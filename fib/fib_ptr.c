
#include "../useful_defs.h"
#include <stdio.h>
#include <gmp.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

//#define MAX 3000
//#define MAX_ITER LONGEST

int main(unsigned int argc, char ** argv) {
    char * count = malloc(CHAR_SIZE);

    // handle args
    size_t max_iter;
    switch (argc){
        case 0: {
            // argc should always be >0
            return 1;
        }
        case 1: {
            // read stdin until newline or EOF
            int in;
            for (; (in = getchar()) != EOF && (in != '\n');){
                append_char(count, (char)in);
            }
            if (in == EOF){
                fprintf( stderr,"\n");
            }
            break;
        }
        case 2: {
            count = argv[1];
            break;
        }
        default:{
            // not yet implemented
            return 2;
        }
    }

    char * end;
    max_iter = strtoull(count, &end, 10);


    // array of mpz structs.
    mpz_t values[3];

    mpz_t * left;
    mpz_t * right;
    mpz_t * result;
    mpz_t * free_ptr;

    left = &values[0];
    right = &values[1];
    result = &values[2];

    // init values
//    *left = 0, *right = 1, *result = 0;
    mpz_init_set_ui(*left, 0);
    mpz_init_set_ui(*right, 1);
    mpz_init_set_ui(*result, 0);


    // start loop
    size_t iter;
    for(iter = 0; iter < max_iter; iter++){
        // print the result.
//        mpz_out_str(stdout,10,*result);
//        printf ("\n\n");

        // initial conditions.
        // the left address will be free after the next calculation.
        free_ptr = left;
        // set left to address of right.
        left = right;
        // set right to address of result.
        right = result;
        // now we must store result in address of left.
        result = free_ptr;
        // calculate result
        mpz_add(*result, *left, *right);
    }
    mpz_out_str(stdout,10,*result);
    printf ("\n");

    return 0;
}
