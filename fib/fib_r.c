//
// Created by neruok on 6/29/18.
//
#include "../useful_defs.h"
#include <stdio.h>
#include <string.h>

// since this is recursive rather than constructive,
// overflow of result may cause non halting.
// this can be mitigated by using arbitrary precision
// libraries such as gmp. But the best approach for
// memory usage would be a constructive algorithm.
void fib1(size_t index, unsigned bigint * result){
    unsigned bigint left;
    unsigned bigint right;
    switch (index){
        case 0:
            *result = 0;
            return;
        case 1:
            *result = 1;
            return;
        case 2:
            *result = 1;
            return;
        default:
            fib1(index-2, &left);
            fib1(index-1, &right);
            *result = left + right;
            return;
        }
}


int main(size_t argc, char ** argv){
    unsigned bigint result;

    char * count = malloc(sizeof(char));

    if (argc > 1){
        count = argv[1];
    }
    size_t max_iter;
    char * end;
    const size_t max_index = strtoull(count, &end, 10);

    fib1(max_index, &result);
    printf("%llu\n", result);
    return 0;
}