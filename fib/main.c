
#include "../useful_defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <printf.h>

int main(size_t argc, char ** argv) {
    char * count = malloc(sizeof(char));

    if (argc > 1){
        count = argv[1];
    }
    size_t max_iter;
    char * end;
    max_iter = strtoull(count, &end, 10);

    mpz_t values[3];
    mpz_init_set_ui(values[0], 0);
    mpz_init_set_ui(values[1], 1);
    mpz_init_set_ui(values[2], 0);

    const unsigned char states[3][3] = {
        {0,1,2},
        {1,2,0},
        {2,0,1},
    };
    unsigned char state;

    // start loop
    size_t iter;
    for(iter = 1; iter <= max_iter; iter++){
        state = (unsigned char) ( ((unsigned char)iter) % 3);
        mpz_add(values[states[state][2]], values[states[state][0]], values[states[state][1]]);
    }
    mpz_out_str(stdout,10,values[states[state][2]]);
    printf ("\n");

    return 0;
}
