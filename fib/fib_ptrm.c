
#include "../useful_defs.h"
#include <stdio.h>
#include <gmp.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

//#define MAX 3000
//#define MAX_ITER LONGEST

int main(unsigned int argc, char ** argv) {
    char * count = malloc(CHAR_SIZE);

    // handle args
    size_t max_iter;
    switch (argc){
        case 0: {
            // argc should always be >0
            return 1;
        }
        case 1: {
            // read stdin until newline or EOF
            int in;
            for (; (in = getchar()) != EOF && (in != '\n');){
                append_char(count, (char)in);
            }
            if (in == EOF){
                fprintf( stderr,"\n");
            }
            break;
        }
        case 2: {
            count = argv[1];
            break;
        }
        default:{
            // not yet implemented
            return 2;
        }
    }

    char * end;
    max_iter = strtoull(count, &end, 10);

    size_t addr_start = &values[0];
    const size_t addr_size = (size_t)&values[1] - addr_start;

    mpz_t left;
    mpz_t right;
    mpz_t result;

    mpz_init_set_ui(left, 0);
    mpz_init_set_ui(right, 1);
    mpz_init_set_ui(result, 0);

    // array of mpz_t pointers.
    mpz_t * pointers[3] = {
            left,right,result
    };
    //array of pointers to those pointers.
    mpzt * addreses = {left, right, result};
    for(char i = 0; i < 3; i++){
        addresses[i] = &pointers[i];
        printf("%lu\n", addreses[i]);
    }



    // start loop
    size_t iter;
    for(iter = 0; iter < max_iter; iter++){
        addreses[0] = addr_start + addr_size<<1;
        addreses[1] = addr_start;
        addreses[2] = addr_start + addr_size;
        // calculate result
        mpz_add(*result, *left, *right);
    }
    mpz_out_str(stdout,10,*result);
    printf ("\n");

    return 0;
}
