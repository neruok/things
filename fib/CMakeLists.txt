add_executable(fib64 fib/fib64.c useful_defs.c)

add_executable(fib_r fib/fib_r.c useful_defs.c)

add_executable(fib_a fib/fib_a.c useful_defs.c)
target_link_libraries(fib_a gmp)

add_executable(fib_mod fib/fib_mod.c useful_defs.c)
target_link_libraries(fib_mod gmp)

add_executable(fib_ptr fib/fib_ptr.c useful_defs.c)
target_link_libraries(fib_ptr gmp)

add_executable(fib_state fib/fib_state.c useful_defs.c)
target_link_libraries(fib_state gmp)

#add_executable(fib_ptrm fib_ptrm.c useful_defs.c)
#target_link_libraries(fib_ptrm gmp)
