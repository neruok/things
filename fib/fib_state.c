
#include "../useful_defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>

//#define MAX 3000
//#define MAX_ITER 30

typedef struct state {
    unsigned char left:2;
    unsigned char right:2;
    unsigned char result:2;
} State;

void rotate_state(State * st){

    *st = (State) {
            .left = st->right,
            .right = st->result,
            .result = st->left
    };

}

void print_state(State * st){
    printf("{%d, %d, %d}\n",
           st->left,
           st->right,
           st->result
    );
}

int main(size_t argc, char ** argv) {
    char * count = malloc(CHAR_SIZE);

    if (argc > 1){
        count = argv[1];
    }
    char * end;
    const size_t max_iter = strtoull(count, &end, 10);

//    unsigned bigint values[3] = {0,1,0};
    mpz_t values[3];
    mpz_init_set_ui(values[0], 0);
    mpz_init_set_ui(values[1], 1);
    mpz_init_set_ui(values[2], 0);

    State * status = malloc(sizeof(State));
    *status = (State) {
            .left = 0,
            .right = 1,
            .result = 2
    };

    // start loop
    size_t iter;
    for(iter = 0; iter <= max_iter; iter++){
//        values[status->result] = values[status->left] + values[status->right];
        mpz_add(values[status->result], values[status->left], values[status->right]);
        rotate_state(status);
    }
    mpz_out_str(stdout,10,values[status->result]);
    printf ("\n");
//    printf("%llu\n", values[status->result]);

    return 0;
}
