
#include "../useful_defs.h"
#include <stdio.h>

//#define MAX 3000
#define MAX_ITER 1000


// Since the address of result changes,
// the memory address may not be on the stack
// of the calling function.
// This means return by value must be used.
// this can either be declared as part of the function
// or copied to the correct address at end.
// performance should be about the same, but syntax
// will differ for usage.
unsigned bigint fib64(size_t index){
    unsigned bigint values[3];
    unsigned bigint *left=&values[0], *right=&values[1], *result=&values[2];
    unsigned bigint *free_ptr;

    // init values
    *left = 0, *right = 1, *result = 0;
    // start loop
    size_t iter;
    for(iter = 1; iter <= index; iter++){
        // Rotate pointers through the array.
        // 0 [0, 1, 2]    1 [0, 1, 2]    2 [0, 1 ,2]
        // f  l, r, R  -> f  R, l, r  -> f  r, R, l
        free_ptr = left;
        left = right;
        right = result;
        result = free_ptr;

        // The result is stored where the left was.
        *result = *left + *right;
    }
    return *result;
}

int main(size_t argc, char ** argv) {

    char * count = malloc(sizeof(char));

    if (argc > 1){
        count = argv[1];
    }
    char * end;
    const size_t idx = strtoull(count, &end, 10);

    unsigned bigint result = fib64(idx);

    printf("%llu\n", result);

    return 0;
}
