#include "../useful_defs.h"
#include <stdio.h>
#include <stdbool.h>

// longest a line may be, 1k is probably big enough.
#define MAXLINE 1024


size_t t_getline(char line[], size_t max);
bigint strindex(char source[], char searchfor[]);

char pattern[] = "ould";

/* find all lines matching pattern */
bigint main() {
    char line[MAXLINE];
    bigint found = 0;

    while (t_getline(line, MAXLINE) > 0){
        if (strindex(line, pattern) >= 0){
            printf("%s", line);
            found ++;
        }
    }
    return found;

}

size_t t_getline(char s[], size_t lim){
   int c;
   size_t i=0;

    //if it isn't EOF, can be safely cast to char.
   while (--lim > 0 && (c=getchar()) != EOF && c !='\n'){
       s[i++] = (char)c;
   }
   if (c == '\n'){
       s[i++] = (char)c;
   }
   s[i] = '\0';
   return i;

}

