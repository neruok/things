//
// Created by neruok on 7/31/18.
//
#include <stdio.h>

#include "../useful_defs.h"

int main(){

    // read stdin until newline or EOF
    int in;

    for (; (in = getchar()) != EOF;){
        fprintf(stdout, "%x ", in );
    }

    fprintf(stdout, "%x ", in );

    fprintf(stderr, "\n\n");

    return 0;
}