//
// Created by neruok on 7/31/18.
//

#include "status.h"

static const int error_codes[ERROR_COUNT] = {
        GOOD,
        UNKNOWN_ERR,
        USAGE_ERR,
        NYI_ERR,
        EOF_ERR,
        DECODE_ERR,
};

static const char *exit_message[ERROR_COUNT] = {
        [GOOD] = "",
        [UNKNOWN_ERR] = "Unknown Error: (%i)\n",
        [USAGE_ERR] = "Usage Error: (%i)\n",
        [NYI_ERR] = "Not yet implemented: (%i)\n",
        [EOF_ERR] = "Reached EOF: (%i)\n",
        [DECODE_ERR] = "Failed Decoding Input: (%i)\n",
};

int ReturnStatus(int error_code){

    if( error_code >= 0 && error_code < ERROR_COUNT){
        fprintf(stderr, exit_message[error_code], error_code);
    }
    else{
        fprintf(stderr, "Extreme Unknown Error! (%i)", error_code);
    }

    return error_code;

}
