//
// Created by neruok on 8/2/18.
//

#ifndef THING_ARGS_H
#define THING_ARGS_H

#include <stdio.h>
#include <stdlib.h>

#include <argp.h>

#include "string.h"

#include "setup.h"

typedef struct argp_option argp_option;
typedef struct argp argp;

void setDefaultSettings();

int args(size_t argc, char ** argv);


#endif //THING_ARGS_H
