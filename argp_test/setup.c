//
// Created by neruok on 8/2/18.
//

#include "setup.h"


typedef struct ProgramSettings{
    bool verbose;
    size_t input_byte_count;
    InputFlags input_mode;
} ProgramSettings;

//default to the first option in the enum for input_mode
static const ProgramSettings DefaultSettings = {
    verbose: false,
    input_byte_count: 128,
    input_mode:  0,
};

// default settings.
ProgramSettings Settings = DefaultSettings;

bool getVerbose(){
    return Settings.verbose;
}

bool setVerbose(bool v){
    Settings.verbose = v;
}


