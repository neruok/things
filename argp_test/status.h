//
// Created by neruok on 7/31/18.
//

#ifndef THING_STATUS_H
#define THING_STATUS_H

#include <stdio.h>

int ReturnStatus(int error_code);

#define ERROR_COUNT 6

#define GOOD 0
#define UNKNOWN_ERR 1
#define USAGE_ERR 2
#define NYI_ERR 3
#define EOF_ERR 4
#define DECODE_ERR 5


#endif //THING_STATUS_H
