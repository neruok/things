//
// Created by neruok on 8/2/18.
//

#ifndef THING_SETUP_H
#define THING_SETUP_H

#include <argp.h>
#include <stdlib.h>

#include <stdbool.h>


typedef enum DataTypes{
    error = 0,
    raw = 1,
    hex = 2,
    b64 = 3,
} __attribute__((__packed__)) DataTypes;

typedef struct InputModes{
    size_t min;
    size_t max;
    enum DataTypes data;
} __attribute__((__packed__)) InputModes;


//bool verbose;
bool getVerbose();
bool setVerbose(bool);

//size_t input_byte_count;
size_t getInputSize();
bool setInputSize(size_t);

//InputFlags input_mode;
int getInputMode();
bool setInputMode(int);


#define IfVerbose() if (getVerbose())

#endif //THING_SETUP_H
