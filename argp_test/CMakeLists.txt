


list(APPEND argp_test_SOURCES
        "argp_test/argp_test.c"
        "argp_test/args.c"
        "argp_test/status.c"
        "argp_test/setup.c"
        )

MESSAGE("${argp_test_SOURCES}")

add_executable(argp_test ${argp_test_SOURCES})


#add_executable(argp_test argp_test/argp_test.c argp_test/args.c)
#target_link_libraries(argp_test argp)
