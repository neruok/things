//
// Created by neruok on 8/2/18.
//

#include "args.h"

static const argp_option options[] = {
        { "format", 'f', "format", 0, "Read stdin with data format...\n"
                                        "\t\"raw\", --raw\n"
                                        "\t\"hex\", --hex\n"
                                        "\t\"b64\", --b64"},
        { "raw", 555, 0, OPTION_HIDDEN, "\tRead stdin as \'raw\' data"},
        { "hex", 556, 0, OPTION_HIDDEN, "\tRead stdin as \'hex\' data"},
        { "b64", 557, 0, OPTION_HIDDEN, "\tRead stdin as \'b64\' data"},
        { "verbose", 'v', 0, 0, "Enable verbose output"},
        { "bytes", 'B', "count", 0, "Byte length of keys"},
        { 0 }
};


static int parse_opt(int key, char *arg, struct argp_state * state){
    switch (key){
        case 'v': {
            setVerbose(true);
            break;
        }
        case 555: {
            setInputMode(raw);
            break;
        }
        case 556: {
            setInputMode(hex);
            break;
        }
        case 557: {
            setInputMode(b64);
            break;
        }
        case 'f': {
            if (strcmp(arg, "raw") == 0){
                setInputMode(raw);
            }
            else if (strcmp(arg, "hex") == 0){
                setInputMode(hex);
            }
            else if (strcmp(arg, "b64") == 0){
                setInputMode(b64);
            }
            else{
                setInputMode(error);
            }
            break;
        }
        case 'B': {
            char * end;
            setInputSize(strtoull(arg, &end, 10));
            break;
        }
    }
    return 0;
}

int args(size_t argc, char ** argv){
    argp argstruct = { options, parse_opt };
    return argp_parse (&argstruct, argc, argv, 0, 0, 0);
}