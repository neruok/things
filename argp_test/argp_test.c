//
// Created by neruok on 8/2/18.
//
#include <stdio.h>

#include "args.h"
#include "status.h"


int main(int argc, char ** argv){
    // initialization.
    int status = args(argc, argv);

    IfVerbose() {
        fprintf(stderr, "Data Type ID: %d\n", getInputMode());
        fprintf(stderr, "Bytes: %llu\n", getInputSize());
    }

    //return ReturnStatus(status);
    return status;
}
