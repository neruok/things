// The MIT License (MIT)
//
// Copyright (c) 2013 Barry Steyn
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "b64.h"

int main() {
    //Encode To Base64
    char* base64EncodeOutput, *text="Hello World";

    Base64Encode(text, strlen(text), &base64EncodeOutput);
    printf("Output (base64): %s\n", base64EncodeOutput);

    //Decode From Base64
    char* base64DecodeOutput;
    size_t test;
    Base64Decode("SGVsbG8gV29ybGQ=", &base64DecodeOutput, &test);
    printf("Output: %s %d\n", base64DecodeOutput, test);

    return(0);
}