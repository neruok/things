//
// Created by neruok on 7/30/18.
//
#include "rsa_gen.h"

#include "../useful_defs.h"

#include "args.h"
#include "prime.h"
#include "settings.h"
#include "status.h"


int main(size_t argc, char ** argv){
    int code_status = 0;
    mpz_t p1;
    mpz_init_set_ui(p1, 0);


    ReadSettings settings = {
        "raw",
        128
    };

    code_status = args(&settings, argc, argv);
    if( code_status != 0 ){
        return ReturnStatus(code_status);
    }

    code_status = get_prime(&p1, &settings);
    if( code_status != 0 ){
        return ReturnStatus(code_status);
    }

    mpz_out_str(stdout,10,p1);
    printf ("\n");



    return 0;
}