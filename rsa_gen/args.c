//
// Created by neruok on 8/1/18.
//

#include "args.h"

int args(ReadSettings * settings, size_t argc, char ** argv){
    size_t action = argc;

    // default to raw input, 1024 bit.
    char * input_format = "raw";
    size_t read_len = 128;

    if(action > 3 || action < 1){
        //usage
        return 2;
    }

    if(action == 3){
        // determine the input format.
        switch (argv[2][0]) {
            case 'x' : {
                input_format = "hex";
                break;
            }
            case 's' : {
                input_format = "b64";
                break;
            }
            case 'r' : {
                input_format = "raw";
                break;
            }
            default: {
                //usage
                return 2;
            }
        }
        //fprintf(stderr, "input_format: %s\n", input_format);
        action = 2;
    }

    if (action == 2){
        char * end;
        read_len = strtoull(argv[1], &end, 10);
        if(read_len < 1){
            //usage
            return 2;
        }
        //fprintf(stderr, "min_length: %llu\n", read_len);
        action = 1;
    }

    if (action == 1) {
        //*settings->method = *input_format;
        strcpy(settings->method, input_format);
        settings->bytes = read_len;
        return 0;
    }
}
