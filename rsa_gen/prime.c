//
// Created by neruok on 8/1/18.
//

#include "prime.h"

#define CHECK1 20
#define CHECK2 2000

static int check_prime(mpz_t * rop){
    int c1 = mpz_probab_prime_p(*rop, CHECK1);
    switch (c1){
        case 0: {
            return 0;
        }
        case 1: {
            return mpz_probab_prime_p(*rop, CHECK2);
        }
        case 2: {
            return 2;
        }
        default:
            return 0;
    }
}

int get_prime(mpz_t * rop, ReadSettings * settings){
    fprintf(stderr, "Read Method: %s\n", settings->method);
    fprintf(stderr, "Bytes to Read: %llu\n", settings->bytes);

    char * method = &settings->method;

    mpz_t candidates[2];
    mpz_init_set_ui(candidates[0], 0);
    mpz_init_set_ui(candidates[1], 0);
    mpz_t *c1, *c2, *result;

    // 2 candidates, c2 = c1+2.
    c1 = &candidates[0];
    c2 = &candidates[1];
    int prime_status[2] = {0,0};

    // we'll generate two numbers to check at once
    // this will help for crappy seeds.
    do {
        read_into_mpz_t(c1, method, settings->bytes);
        // haiku:
        // if it is even...
        // we should make the number odd
        // by subtracting one
        if (mpz_even_p(*c1) > 0){
            mpz_sub_ui(*c1, *c1, 1);
        }

        // set c2
        mpz_add_ui(*c2, *c1, 2);

        // check them both.
        prime_status[0] = check_prime(c1);

        // no point in checking
        // the second if we found
        // a definite prime.
        if (prime_status[0] == 2){
            result = c1;
            break;
        }

        prime_status[1] = check_prime(c2);

        //no possible primes found.
        if ( (prime_status[0] == 0) && (prime_status[1] ==0) ){
            continue;
        }

        if (prime_status[1] == 2){
            result = c2;
            break;
        }

        if (prime_status[0] == 1){
            result = c1;
            break;
        }
        else if (prime_status[1] == 1){
            result = c2;
            break;
        }


    }
    while(1);

    mpz_set(*rop, *result);

    return (0);
}