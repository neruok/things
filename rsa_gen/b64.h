//
// Created by neruok on 7/30/18.
//

#ifndef THING_B64_H
#define THING_B64_H

#include <stdio.h>

int Base64Decode(char* b64message, unsigned char** buffer, size_t* length);
int Base64Encode(const unsigned char* buffer, size_t length, char** b64text);

#endif //THING_B64_H
