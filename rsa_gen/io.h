//
// Created by neruok on 7/31/18.
//

#ifndef THING_IO_H
#define THING_IO_H

#include <stdio.h>
#include <stdlib.h>
//#include <stdbool.h>

#include <gmp.h>

char * read(size_t bytes_len);
void read_into_mpz_t(mpz_t * rop, char * method, size_t len);

#endif //THING_IO_H
