//
// Created by neruok on 8/1/18.
//

#ifndef THING_PRIME_H
#define THING_PRIME_H

#include <stdio.h>

#include <gmp.h>

#include "settings.h"
#include "io.h"
//#include "status.h"

int get_prime(mpz_t *, ReadSettings *);

#endif //THING_PRIME_H
