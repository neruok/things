//
// Created by neruok on 7/31/18.
//
#include "io.h"

#include "../useful_defs.h"
#include "b64.h"
#include "status.h"

char * read(size_t bytes_min_len){
    char * temp = malloc(CHAR_SIZE*(bytes_min_len));

    int in;
    size_t read_bytes = 0;
    while ( 1 ){
        in = getchar();

        temp[read_bytes] = (char) in;
        read_bytes += 1;
        if (read_bytes == bytes_min_len){
            break;
        }
    }
    return temp;
}

// converts the value to hex and reads into a gmp integer.
void read_into_mpz_t(mpz_t * rop, char * method, size_t len){

    char * input = read(len);
    char * hex_coded = "";

    if (strcmp(method, "raw") == 0){
        // must convert raw to hex string.
        //fprintf(stderr, "encoding:\n%s\n", input);
        hex_coded = hex_encode_raw(input, len);
        //fprintf(stderr, "encoded:\n%s\n", hex_coded);
    }
    else if (strcmp(method, "hex") == 0){
        // hey, it's already hex!
        hex_coded = input;
    }
    else if (strcmp(method, "b64") == 0){
        // Decode b64 to raw.
        unsigned char * base64DecodeOutput;
        size_t test;
        Base64Decode(input, &base64DecodeOutput, &test);

        // encode raw as hex
        hex_coded = hex_encode_raw(base64DecodeOutput, test);
    }

    if (strcmp(hex_coded, "") != 0) {
        mpz_set_str(*rop, hex_coded, 16);
    }
    return;
}
