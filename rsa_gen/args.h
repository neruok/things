//
// Created by neruok on 8/1/18.
//

#ifndef THING_ARGS_H
#define THING_ARGS_H

#include <string.h>
#include <stdio.h>

//#include "status.h"
#include "io.h"
#include "settings.h"

int args(ReadSettings * settings, size_t argc, char ** argv);

#endif //THING_ARGS_H
