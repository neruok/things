//
// Created by neruok on 7/31/18.
//

#include "status.h"

#define ERROR_COUNT 6

static const int error_codes[ERROR_COUNT] = {
        0, 1, 2, 3, 4, 5
};

static const char *exit_message[ERROR_COUNT] = {
        [ 0] = "\n",
        [ 1] = "Unknown Error: (%i)\n",
        [ 2] = "Usage Error: (%i)\n",
        [ 3] = "Not yet implemented: (%i)\n",
        [ 4] = "Reached EOF: (%i)\n",
        [ 5] = "Failed Decoding Input: (%i)\n",
};

int ReturnStatus(int error_code){

    if( error_code >= 0 && error_code < ERROR_COUNT){
        fprintf(stderr, exit_message[error_code], error_code);
    }
    else{
        fprintf(stderr, "Extreme Unknown Error! (%i)", error_code);
    }

    return error_code;

}
