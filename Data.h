//
// Created by neruok on 8/1/18.
//

#ifndef THING_DATA_H
#define THING_DATA_H

typedef struct RawData{
    size_t size;
    unsigned char * data;
} RawData;

#endif //THING_DATA_H
