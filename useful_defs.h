//
// Created by neruok on 6/7/18.
//
#ifndef THING_USEFUL_DEFS_H
#define THING_USEFUL_DEFS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>


#define bigint long long int
#define bigfloat long double

#define TRUE 1
#define FALSE 0

#define SHORTEST ( (size_t)0 )
#define LONGEST ( ((size_t)0) - 1 )

#define pass ((void)0)
#define NOOP ((void)0)

#define CHAR_SIZE (sizeof(char))

void append_char(char *, char);

char * byte_to_bin(unsigned char *);

char * byte_to_hex(unsigned char * input);

char * hex_encode_raw(char * bytes, size_t len);

char * hex_encode(char * string);

char * oct_to_b64(char * string);

bool isValueInArray(int * val, int *arr, int size);

#endif //THING_USEFUL_DEFS_H
