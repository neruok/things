//
// Created by neruok on 6/21/18.
//
#include "useful_defs.h"
#include <stdio.h>
#include <stdbool.h>

void append_char(char * left, char right){
    // copy the old string into temp.
    char * temp = malloc(sizeof(*left));
    strcpy(temp, left);

    // assign more memory for left.
    size_t len = strlen(left);
    left = realloc(left, (len+1+1) * CHAR_SIZE);

    strcpy(left, temp);
    free(temp);
    left[len] = right;
    left[len+1] = '\0';
}


static const char *bit_rep[16] = {
    [ 0] = "0000", [ 1] = "0001", [ 2] = "0010", [ 3] = "0011",
    [ 4] = "0100", [ 5] = "0101", [ 6] = "0110", [ 7] = "0111",
    [ 8] = "1000", [ 9] = "1001", [10] = "1010", [11] = "1011",
    [12] = "1100", [13] = "1101", [14] = "1110", [15] = "1111",
};

char * byte_to_bin(unsigned char *byte)
{
    char * val = malloc(9*CHAR_SIZE);
    char * h1 = val;
    char * h2 = val + (4*CHAR_SIZE);
    strcpy(h1, bit_rep[*byte >> 4]);
    strcpy(h2, bit_rep[*byte & 0x0F]);
    val[8] = '\0';
    return val;
}

static const char hex_rep[16] = {
        [ 0] = '0', [ 1] = '1', [ 2] = '2', [ 3] = '3',
        [ 4] = '4', [ 5] = '5', [ 6] = '6', [ 7] = '7',
        [ 8] = '8', [ 9] = '9', [10] = 'a', [11] = 'b',
        [12] = 'c', [13] = 'd', [14] = 'e', [15] = 'f',
};

char * byte_to_hex(unsigned char *byte){
    char * val = malloc(3*CHAR_SIZE);
    val[0] = hex_rep[*byte >> 4];
    val[1] = hex_rep[*byte & 0x0F];
    val[2] = '\0';
    return val;
}

char * hex_encode_raw(char * bytes, size_t size) {
    // result will be 2x the byte length.
    char * hexout = malloc((CHAR_SIZE*((size*2)+1)));
    hexout[0] = '\0';

    size_t offset = 0;

    // REALLY needs to be unsigned.
    unsigned char * byte = &bytes[0];
    unsigned char * end = &bytes[size];

    while(byte != end){
        hexout[offset] = hex_rep[*byte>>4];
        hexout[offset+1] = hex_rep[*byte & 0x0F];
        hexout[offset+2] = '\0';
        offset+=2;
        byte++;
    }

    return hexout;
}

char * hex_encode(char * string){
    // assign more memory for left.
    char * hexout = malloc(CHAR_SIZE);
    hexout[0] = '\0';

    size_t len = 1;
    size_t next_size;

    size_t offset = 0;

    // REALLY needs to be unsigned.
    unsigned char * byte = &string[0];

    while(*byte != '\0'){
        len += 2;
        next_size = (len) * CHAR_SIZE;
        //printf("next_size: %llu\n", next_size);
        hexout = realloc(hexout, next_size);

        hexout[offset] = hex_rep[*byte>>4];
        hexout[offset+1] = hex_rep[*byte & 0x0F];
        hexout[offset+2] = '\0';
        offset+=2;
        byte++;
    }

    return hexout;
}


bool isValueInArray(int * val, int *arr, int size){
    int i;
    for (i=0; i < size; i++) {
        if (*val == arr[i]) {
            return true;
        }
    }
    return false;
}

// returns non-null-terminated string with length.
//char * hex_to_raw(char * hex, size_t * data_size){
//    size_t hexlength = strlen(hex);
//    if((hexlength % 2) == 1){
//        return "";
//    }
//    *data_size = strlen(hex)/2;
//    size_t offset = 0;
//
//    // ull is 64 bits.
//    char * substr = malloc(CHAR_SIZE*(2+1));
//    substr[3] = '\0';
//    unsigned  byte;
//
//    if (strlen(hex) < 1){
//        return "";
//    }
//
//    do{
//        //copy 2 characters from hex into substr
//        strto(0)
//        // if the string has
//        if (strlen(substr) < 2){
//            break;
//        }
//    }
//    while( 1 );
//}
