//
// Created by neruok on 6/29/18.
//
#include "../useful_defs.h"
#include <stdio.h>
#include <gmp.h>
#include <string.h>

size_t BIT_PRECISION;
size_t DEC_PRECISION;

// estimates the square root by
// shifting off the least significant half
// of the significant bits.
// 0b00110100 == 52 to
// 0b00000110 == 6
//
// 0b10000001 == 129
// 0b00001000 == 8
void sqrt_est(mpz_t rop, mpz_t op1){
    size_t size = mpz_sizeinbase(op1, 2);
    size_t half = size >> 1;
    mpz_div_2exp(rop, op1, half);
    if (mpz_cmp_ui(rop, 0) == 0){
        mpz_set_ui(rop, 1);
    }
}

void average(mpf_t rop, mpf_t op1, mpf_t op2){
    mpf_add(rop, op1, op2);
    mpf_div_2exp(rop, rop, 1);
}

// performs 1 iteration of babylon square root.
void sqrt_babl_f(mpf_t rop, mpf_t s, mpf_t est){
    mpf_t x1;
    mpf_init2(x1, BIT_PRECISION);
    mpf_div(x1, s, est);

    average(rop, est, x1);
}

void sqrt_babl(mpf_t rop, mpz_t op1, size_t iters){
    // define and initialize variables
    mpz_t guess_i;
    mpz_init(guess_i);

    mpf_t guesses[3];

    mpf_t * guess_new = & guesses[0];
    mpf_t * guess_prev = & guesses[1];
    mpf_t * guess_last = & guesses[2];
    mpf_t * guess_free;

    mpf_init2(*guess_new, BIT_PRECISION);
    mpf_init2(*guess_prev, BIT_PRECISION);
    mpf_init2(*guess_last, BIT_PRECISION);


    // set the integer estimate
    sqrt_est(guess_i, op1);
    mpf_set_z(*guess_prev, guess_i);


    // set s
    mpf_t s;
    mpf_init2(s, BIT_PRECISION);
    mpf_set_z(s, op1);

    size_t idx;
    for(idx=0; idx < iters; ++idx) {
        fprintf(stderr, "Iteration: %llu\n", idx);
        //calculate new.
        sqrt_babl_f(*guess_new, s, *guess_prev);

        //compare new to last.
        if (mpf_cmp(*guess_new, *guess_last) == 0){
            fprintf(stderr, "Result Loop: exit\n");
            break;
        }
        //compare new to prev
        else if (mpf_cmp(*guess_new, *guess_prev) == 0){
            fprintf(stderr, "Result Repeated: exit\n");
            break;
        }
        else if (iters-1 == idx){
            fprintf(stderr, "Max Iterations reached\n");
            break;
        }

        //rotate
        // ? => [0,   1,    2]    => 2 => [0,    1,    2]
        // f => [new, prev, last] => f => [prev, last, new]
        guess_free = guess_last;
        guess_last = guess_prev;
        guess_prev = guess_new;
        guess_new = guess_free;

    }

    *rop =  ** guess_new;
}

int main(size_t argc, char ** argv){
    char * input_num;
    char * iterations;
    char * b_precision;
    char * d_precision;

    // handle args
    switch (argc) {
        case 5: {
            // argv[0] is the name of program
            input_num = argv[1];
            iterations = argv[2];
            b_precision = argv[3];
            d_precision = argv[4];
            break;
        }
        default: {
            // not yet implemented
            printf("Usage:\n"
                   "%s [input_num] [iterations] [binary_precision] [decimal_precision]\n",
                    argv[0]);
            return 1;
        }
    }

    mpz_t input;
    size_t max_iter;

    // initialize with
    char * end;
    // initialize max loop
    max_iter = strtoull(iterations, &end, 10);
    // initialize global variables
    DEC_PRECISION = strtoull(d_precision, &end, 10);
    BIT_PRECISION = strtoull(b_precision, &end, 10);

    mpz_init_set_str(input, input_num, 10);

    mpf_t result;
    mpf_init2(result, BIT_PRECISION);
    sqrt_babl(result, input, max_iter);

    gmp_printf("%.*Ff\n", DEC_PRECISION, result);

    return 0;
}